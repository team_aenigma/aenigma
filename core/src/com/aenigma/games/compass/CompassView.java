package com.aenigma.games.compass;

import com.aenigma.games.GameScene;
import com.aenigma.games.Utils;
import com.aenigma.games.compass.model.CardinalPoint;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

public class CompassView extends GameScene
{
    private BitmapFont normalFont;
    private BitmapFont spotlightFont;


    private final int ARROW_SIZE = 300;
    private final int MARGIN_HUD = 100;
    private final int DIRECTION_FONTSIZE = 300;
    private final int DEFAULT_FONTSIZE = 100;
    private final Color DIRECTION_FONTCOLOR = Color.valueOf("70877F");
    private final Color DEFAULT_FONTCOLOR = Color.valueOf("BFC2B1");
    private final Color GAME_BACKGROUND = Color.valueOf("E2E5D1");
    private final Color TIMER_BACKGROUND = Color.valueOf("BFC2B1");
    private final Color TIMER_FOREGROUND = Color.valueOf("3D3234");
    private final Color SCORE_BACKGROUND = Color.valueOf("BFC2B1");
    private final Color SCORE_FOREGROUND = Color.valueOf("12C4C2");


    public CompassView()
    {
        // Generate fonts
        spotlightFont = Utils.generateFont("fonts/OldSansBlack.ttf", DIRECTION_FONTSIZE, DIRECTION_FONTCOLOR);
        normalFont = Utils.generateFont("fonts/OldSansBlack.ttf", DEFAULT_FONTSIZE, DEFAULT_FONTCOLOR);
    }

    public void render(CompassLogic logic)
    {
        // Set background color
        Color background = GAME_BACKGROUND;
        Gdx.gl.glClearColor(background.r, background.g, background.b, background.a);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        // Update display components
        updateComponents();

        int sequenceSize = logic.getSequenceSize();

        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        spriteBatch.begin();

        switch (logic.getState())
        {
            case ONGOING:
                final String targetDirection = getCPString(logic.getTargetCP());
                final float targetDirectionWidth = spotlightFont.getBounds(targetDirection).width;

                // Drawing arrow
                shapeRenderer.setColor(getCPColor(logic.getTargetCP(), logic.getCurrentCP()));
                shapeRenderer.rect(-ARROW_SIZE / 2, -ARROW_SIZE / 2, ARROW_SIZE, ARROW_SIZE);
                shapeRenderer.setColor(background);
                shapeRenderer.triangle(-ARROW_SIZE / 2, +ARROW_SIZE / 2, -ARROW_SIZE / 2, -ARROW_SIZE / 2, 0, +ARROW_SIZE / 2);
                shapeRenderer.triangle(+ARROW_SIZE / 2, +ARROW_SIZE / 2, +ARROW_SIZE / 2, -ARROW_SIZE / 2, 0, +ARROW_SIZE / 2);
                shapeRenderer.triangle(-ARROW_SIZE / 2, -ARROW_SIZE / 2, +ARROW_SIZE / 2, -ARROW_SIZE / 2, 0, -ARROW_SIZE / 6);

                double elapsedTime = logic.getElapsedTime();

                final float COMMON_WIDTH = 30;
                final float COMMON_HEIGHT = -viewport.getWorldHeight() + MARGIN_HUD;
                final float POS_X = viewport.getWorldWidth() / 2 - MARGIN_HUD / 2;
                final float POS_Y = viewport.getWorldHeight() / 2 - MARGIN_HUD / 2;
                final float timerFg = logic.getTimerSize(COMMON_HEIGHT, elapsedTime);

                // Draw timer
                shapeRenderer.setColor(TIMER_BACKGROUND);
                shapeRenderer.rect(-POS_X, POS_Y, COMMON_WIDTH, COMMON_HEIGHT);
                shapeRenderer.setColor(TIMER_FOREGROUND);
                shapeRenderer.rect(-POS_X, -POS_Y, COMMON_WIDTH, -timerFg);

                // Draw score
                shapeRenderer.setColor(SCORE_BACKGROUND);
                shapeRenderer.rect(POS_X - COMMON_WIDTH / 2, POS_Y, COMMON_WIDTH, COMMON_HEIGHT);
                shapeRenderer.setColor(SCORE_FOREGROUND);
                shapeRenderer.rect(POS_X - COMMON_WIDTH / 2, -POS_Y, COMMON_WIDTH, -logic.getScore() * COMMON_HEIGHT / sequenceSize);

                // Display target direction
                spotlightFont.draw(spriteBatch, targetDirection, -targetDirectionWidth / 2, viewport.getWorldHeight() / 2 - MARGIN_HUD);

                // Write Score label
                float scoreWidth = normalFont.getBounds("score").width;
                float scoreHeight = normalFont.getBounds("score").height;
                normalFont.draw(spriteBatch, "score", viewport.getWorldWidth() / 2 - scoreWidth - 100,
                                -viewport.getWorldHeight() / 2 + scoreHeight + 50);

                // Write Timer label
                float timerLabelHeight = normalFont.getBounds("timerLabel").height;
                normalFont.draw(spriteBatch, "time", -viewport.getWorldWidth() / 2 + 100,
                                -viewport.getWorldHeight() / 2 + timerLabelHeight + 50);
                break;

            case ENDED:
                String scoreText = Math.round(logic.getScore()) + "/" + sequenceSize;
                BitmapFont.TextBounds scoreSize = normalFont.getBounds(scoreText);
                normalFont.draw(spriteBatch, scoreText, -scoreSize.width / 2,
                        -scoreSize.height / 2);
                break;
        }

        spriteBatch.end();
        shapeRenderer.end();
    }

    private String getCPString(CardinalPoint point)
    {
        switch(point)
        {
            case NORTH:
                return "N";
            case NORTH_EAST:
                return "NE";
            case NORTH_WEST:
                return "NW";
            case EAST:
                return "E";
            case WEST:
                return "W";
            case SOUTH_WEST:
                return "SW";
            case SOUTH_EAST:
                return "SE";
            case SOUTH:
                return "S";
            default:
                return "";
        }
    }

    private Color getCPColor(CardinalPoint target, CardinalPoint current)
    {
        Color good = Color.valueOf("739102");
        Color almost = Color.valueOf("FFBC00");
        Color meh = Color.valueOf("E27A3F");
        Color bad = Color.valueOf("EE0807");

        if (target == current)
            return good;

        switch (target)
        {
            case NORTH:
                if (current == CardinalPoint.SOUTH)
                    return bad;
                else if (current == CardinalPoint.SOUTH_EAST
                        || current == CardinalPoint.SOUTH_WEST
                        || current == CardinalPoint.WEST
                        || current == CardinalPoint.EAST)
                    return meh;
                else if (current == CardinalPoint.NORTH_EAST
                        || current == CardinalPoint.NORTH_WEST)
                    return almost;
            case SOUTH:
                if (current == CardinalPoint.NORTH)
                    return bad;
                else if (current == CardinalPoint.NORTH_EAST
                        || current == CardinalPoint.NORTH_WEST
                        || current == CardinalPoint.WEST
                        || current == CardinalPoint.EAST)
                    return meh;
                else if (current == CardinalPoint.SOUTH_EAST
                        || current == CardinalPoint.SOUTH_WEST)
                    return almost;
            case EAST:
                if (current == CardinalPoint.WEST)
                    return bad;
                else if (current == CardinalPoint.NORTH_WEST
                        || current == CardinalPoint.SOUTH_WEST
                        || current == CardinalPoint.NORTH
                        || current == CardinalPoint.SOUTH)
                    return meh;
                else if (current == CardinalPoint.NORTH_EAST
                        || current == CardinalPoint.SOUTH_EAST)
                    return almost;
                case WEST:
                if (current == CardinalPoint.EAST)
                    return bad;
                else if (current == CardinalPoint.NORTH_EAST
                        || current == CardinalPoint.SOUTH_EAST
                        || current == CardinalPoint.NORTH
                        || current == CardinalPoint.SOUTH)
                    return meh;
                else if (current == CardinalPoint.NORTH_WEST
                        || current == CardinalPoint.SOUTH_WEST)
                    return almost;
        }

        return Color.BLACK;
    }

}
