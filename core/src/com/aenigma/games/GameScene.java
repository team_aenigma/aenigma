package com.aenigma.games;

import com.aenigma.games.touch.TouchLogic;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

public abstract class GameScene
{
    protected Viewport viewport;
    protected Camera camera;
    protected ShapeRenderer shapeRenderer;
    protected SpriteBatch spriteBatch;

    public GameScene()
    {
        camera = new OrthographicCamera();
        viewport = new ScreenViewport(camera);
        shapeRenderer = new ShapeRenderer();
        spriteBatch = new SpriteBatch();
    }

    public void updateComponents()
    {
        camera.update();
        shapeRenderer.setProjectionMatrix(camera.combined);
        spriteBatch.setProjectionMatrix(camera.combined);
    }

    public SpriteBatch getSpriteBatch()
    {
        return spriteBatch;
    }

    public void updateViewport(int width, int height)
    {
        viewport.update(width, height);
    }
}
