package com.aenigma.games.touch.model;

import com.badlogic.gdx.math.Vector2;

public class Target
{
    private Vector2 position;
    private float width;
    private float height;

    public Target()
    {
        position = new Vector2(0, 0);
        width = 0;
        height = 0;
    }

    public Target(Vector2 position, float width, float height)
    {
        this.position = position;
        this.width = width;
        this.height = height;
    }

    public Vector2 getSize()
    {
        return new Vector2(width, height);
    }

    public void setSize(float width)
    {
        this.width = width;
    }

    public void setSize(float width, float height)
    {
        this.width = width;
        this.height = height;
    }

    public Vector2 getPosition()
    {
        return position;
    }

    public void setPosition(float x, float y)
    {
        position.x = x;
        position.y = y;
    }
}
