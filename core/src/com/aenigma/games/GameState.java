package com.aenigma.games;

public enum GameState
{
    INITIALIZING,
    ONGOING,
    FEEDBACK,
    ENDED
};
