package com.aenigma.games.audio;

import com.aenigma.games.GameState;
import com.aenigma.games.audio.model.AudioPlayer;
import com.aenigma.games.audio.model.DiskState;
import com.aenigma.games.audio.model.Timer;
import com.badlogic.gdx.math.Vector2;

import java.util.ArrayList;

public class AudioLogic
{
    private GameState state;
    private Timer timer;

    AudioPlayer aplayer;
    ArrayList<DiskState> playerSequence;
    ArrayList<DiskState> actualSequence;

    final int TIME = 10000;

    float worldWidth;
    float worldHeight;

    public AudioLogic()
    {
        state = GameState.INITIALIZING;

        aplayer = new AudioPlayer();
        playerSequence = new ArrayList<DiskState>();
        timer = new Timer(TIME);
    }

    public void setGame(int width, int height)
    {
        actualSequence = aplayer.getSequence();
        playerSequence.clear();
        playerSequence.add(actualSequence.get(0));
        for (int i = 1; i < 5; i++)
            playerSequence.add(DiskState.NONE);

        worldWidth = width;
        worldHeight = height;
    }

    private boolean checkSequence()
    {
        boolean result = true;

        for (int i = 0; i < actualSequence.size(); i++)
        {
            if (actualSequence.get(i) != playerSequence.get(i))
                result = false;
        }

        return result;
    }

    public void update()
    {
        if (!timer.hasStarted() && aplayer.getCount() == 1)
        {
            timer.start();
            state = GameState.ONGOING;
        }

        if (timer.hasStarted() && (checkSequence() || timer.getElapsedTime() > TIME))
            state = GameState.ENDED;
    }

    public void touchDisk(int i)
    {
        DiskState state = playerSequence.get(i);

        switch (state)
        {
            case NONE:
                playerSequence.set(i, DiskState.SOUND1);
                break;
            case SOUND1:
                playerSequence.set(i, DiskState.SOUND2);
                break;
            case SOUND2:
                playerSequence.set(i, DiskState.SOUND1);
                break;
        }
    }

    public ArrayList<DiskState> getPlayerSequence()
    {
       return playerSequence;
    }

    public AudioPlayer getPlayer()
    {
        return aplayer;
    }

    public GameState getState()
    {
        return state;
    }

    public long getElapsedTime()
    {
        return timer.getElapsedTime();
    }

    public Vector2 getWorldDimension()
    {
        return new Vector2(worldWidth, worldHeight);
    }

    public int getMaxTime()
    {
        return TIME;
    }
}
