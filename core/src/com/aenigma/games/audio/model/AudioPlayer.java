package com.aenigma.games.audio.model;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;

import java.util.ArrayList;
import java.util.Random;

public class AudioPlayer
{
    Sound s;
    Sound s2;
    ArrayList<DiskState> diskSequence;
    ArrayList<Sound> sequence;
    int repeatCount;
    boolean isPlaying = false;


    public AudioPlayer()
    {
        s = Gdx.audio.newSound(Gdx.files.internal("sounds/sound1.mp3"));
        s2 = Gdx.audio.newSound(Gdx.files.internal("sounds/sound2.mp3"));

        repeatCount = 0;
        diskSequence = new ArrayList<DiskState>();
        sequence = new ArrayList<Sound>();
        generateSequence(5);
    }

    private void generateSequence(int size)
    {
        sequence.clear();

        // Fill sequence randomly
        for (int i = 0; i < size; i++)
        {
            if (new Random().nextInt() % 2 == 0)
            {
                sequence.add(s);
                diskSequence.add(DiskState.SOUND1);
            }
            else
            {
                sequence.add(s2);
                diskSequence.add(DiskState.SOUND2);
            }
        }
    }

    public void playSequence()
    {
        isPlaying = true;
        repeatCount++;
        long startTime = System.currentTimeMillis();
        sequence.get(0).play();

        for (int i = 1; i < sequence.size(); i++)
        {
            if (System.currentTimeMillis() - startTime > 700)
            {
                startTime = System.currentTimeMillis();
                sequence.get(i).play();
            }
            else
                i--;
        }
        isPlaying = false;
    }

    public void disposeSound()
    {
        s.dispose();
        s2.dispose();
    }

    public ArrayList<DiskState> getSequence()
    {
        return diskSequence;
    }

    public int getCount()
    {
        return repeatCount;
    }

    public boolean isPlaying()
    {
        return isPlaying;
    }
}
