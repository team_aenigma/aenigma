package com.aenigma.games.audio.model;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;

public class Disk
{
    private DiskState state;
    private Vector2 position;
    private float size;
    private Texture texture;

    public Disk(Vector2 position, float size, DiskState state, Texture texture)
    {
        this.position = position;
        this.size = size;
        this.state = state;
        this.texture = texture;
    }

    public Vector2 getPosition()
    {
        return position;
    }

    public void setPosition(Vector2 position)
    {
        this.position = position;
    }

    public float getSize()
    {
        return size;
    }

    public void setSize(float size)
    {
        this.size = size;
    }

    public DiskState getState()
    {
        return state;
    }

    public Texture getTexture()
    {
        return texture;
    }
}
