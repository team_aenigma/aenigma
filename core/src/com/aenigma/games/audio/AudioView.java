package com.aenigma.games.audio;

import com.aenigma.games.GameScene;
import com.aenigma.games.Utils;
import com.aenigma.games.audio.model.Disk;
import com.aenigma.games.audio.model.DiskState;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;

import java.util.ArrayList;
import java.util.Set;

public class AudioView extends GameScene
{
    Texture repeatAudio;
    Texture disk_none;
    Texture disk_1;
    Texture disk_2;

    Vector2 bloc1;
    Vector2 bloc2;
    Vector2 bloc3;
    Vector2 repeatAudioPosition;

    ArrayList<Disk> disks;

    final Color BLOC1_BACKGROUND = Color.valueOf("1d638e");
    final Color BLOC2_BACKGROUND = Color.valueOf("3884a3");
    final Color BLOC3_BACKGROUND = Color.valueOf("7a9fa8");
    final Color TIMER_FONT = Color.valueOf("1d638e");

    BitmapFont font;

    public AudioView()
    {
        repeatAudio = new Texture(Gdx.files.internal("games/audio_repeat.png"));
        disk_none = new Texture(Gdx.files.internal("games/audio_disk_none.png"));
        disk_1 = new Texture(Gdx.files.internal("games/audio_disk_1.png"));
        disk_2 = new Texture(Gdx.files.internal("games/audio_disk_2.png"));
        font = Utils.generateFont("fonts/AldotheApache.ttf", 100, TIMER_FONT);
        disks = new ArrayList<Disk>();
    }

    private void initBlocPosition()
    {
        if (bloc1 != null)
            return;

        float worldWidth = viewport.getWorldWidth();
        float worldHeight = viewport.getWorldHeight();

        float X = -worldWidth / 2;
        float b1Y = worldHeight / 2 - (worldHeight / 3);
        float b2Y = worldHeight / 2 - 2 * (worldHeight / 3);
        float b3Y = -worldHeight / 2;

        bloc1 = new Vector2(X, b1Y);
        bloc2 = new Vector2(X, b2Y);
        bloc3 = new Vector2(X, b3Y);

        float repeatSize = worldHeight / 4;
        repeatAudioPosition = new Vector2(-repeatSize / 2, bloc1.y + (worldHeight / 3 - repeatSize) / 2);
    }

    public void render(AudioLogic logic)
    {
        initBlocPosition();
        float width = viewport.getWorldWidth();
        float height = viewport.getWorldHeight();

        Color background = Color.valueOf("FF0000");

        // Set background color
        Gdx.gl.glClearColor(background.r, background.g, background.b, background.a);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        // Update display components (camera, batch, renderer...)
        updateComponents();

        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);

        // Render backgrounds
        shapeRenderer.setColor(BLOC1_BACKGROUND);
        shapeRenderer.rect(bloc1.x, bloc1.y, width, height / 3);
        shapeRenderer.setColor(BLOC2_BACKGROUND);
        shapeRenderer.rect(bloc2.x, bloc2.y, width, height / 3);
        shapeRenderer.setColor(BLOC3_BACKGROUND);
        shapeRenderer.rect(bloc3.x, bloc3.y, width, height / 3);

        // Render timer part
        shapeRenderer.end();

        // Draw text and sprite
        spriteBatch.begin();

        float repeatSize = viewport.getWorldHeight() / 4;
        spriteBatch.draw(repeatAudio, repeatAudioPosition.x, repeatAudioPosition.y, repeatSize, repeatSize);

        // Render choices part
        updateDisks(logic.getPlayerSequence());
        for (Disk d : disks)
            spriteBatch.draw(d.getTexture(), d.getPosition().x, d.getPosition().y, d.getSize(), d.getSize());

        // Draw timer
        int timeLeft = (int) logic.getMaxTime() / 1000 - (int) (logic.getElapsedTime() / 1000);
        String timer = (timeLeft < 10) ? "0:0" + timeLeft : "0:" + timeLeft;
        float timerWidth = font.getBounds(timer).width;

        font.draw(spriteBatch, timer, -timerWidth / 2, bloc3.y + viewport.getWorldHeight() / 6);

        spriteBatch.end();
    }

    private void updateDisks(ArrayList<DiskState> states)
    {
        disks.clear();

        float size = viewport.getWorldWidth() / 7;
        float x = -viewport.getWorldWidth() / 2 + size;
        float y = -size / 2;

        for (int i = 0; i < states.size(); i++)
        {
            DiskState disk = states.get(i);
            switch (disk)
            {
                case NONE:
                    disks.add(new Disk(new Vector2(x + size * i, y), size, disk, disk_none));
                    break;
                case SOUND1:
                    disks.add(new Disk(new Vector2(x + size * i, y), size, disk, disk_1));
                    break;
                case SOUND2:
                    disks.add(new Disk(new Vector2(x + size * i, y), size, disk, disk_2));
                    break;
            }

        }
    }

    public Texture getTexture()
    {
        return repeatAudio;
    }

    public Vector2 getTexturePosition()
    {
        return repeatAudioPosition;
    }

}
