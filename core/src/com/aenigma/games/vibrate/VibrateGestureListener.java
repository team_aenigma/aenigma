package com.aenigma.games.vibrate;

import com.aenigma.games.GameState;
import com.aenigma.games.vibrate.model.Button;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.math.Vector2;

import java.util.ArrayList;

public class VibrateGestureListener implements GestureDetector.GestureListener
{
    private VibrateLogic logic;
    private ArrayList<Button> buttons;
    private boolean enableVibrate;

    public VibrateGestureListener(VibrateLogic logic)
    {
        this.logic = logic;
        enableVibrate = true;
    }

    public void setButtons(ArrayList<Button> buttons)
    {
        this.buttons = buttons;
    }

    @Override
    public boolean touchDown(float x, float y, int pointer, int button)
    {
        return false;
    }

    @Override
    public boolean tap(float x, float y, int count, int button)
    {
        if (logic.getState() == GameState.ENDED)
            return false;

        float new_x = - logic.getWorldDimension().x / 2 + x;
        float new_y = logic.getWorldDimension().y / 2 - y + 300; // black magic here

        for (Button b : buttons)
        {
            if ((b.getPosition().x <= new_x && new_x <= b.getPosition().x + b.getSize())
                && (b.getPosition().y <= new_y && new_y <= b.getPosition().y + b.getSize()))
            {
                System.out.println(b.getText());
                if (b.getText().equals("v"))
                    logic.checkValidity();
                else if (b.getText().equals("r"))
                    logic.resetCurrentCode();
                else
                {
                    ArrayList<Long> vibrate = logic.getHints(Integer.parseInt(b.getText()));

                    if (enableVibrate && !vibrate.isEmpty())
                    {
                        long[] result = new long[vibrate.size()];

                        for (int i = 0; i < vibrate.size(); i++)
                            result[i] = vibrate.get(i);

                        Gdx.input.cancelVibrate();
                        Gdx.input.vibrate(result, -1);
                    }
                }
            }
        }
        return false;
    }

    @Override
    public boolean longPress(float x, float y)
    {
        enableVibrate = !enableVibrate;
        return false;
    }

    @Override
    public boolean fling(float velocityX, float velocityY, int button)
    {
        return false;
    }

    @Override
    public boolean pan(float x, float y, float deltaX, float deltaY)
    {
        return false;
    }

    @Override
    public boolean panStop(float x, float y, int pointer, int button)
    {
        return false;
    }

    @Override
    public boolean zoom(float initialDistance, float distance)
    {
        return false;
    }

    @Override
    public boolean pinch(Vector2 initialPointer1, Vector2 initialPointer2, Vector2 pointer1, Vector2 pointer2)
    {
        return false;
    }
}
