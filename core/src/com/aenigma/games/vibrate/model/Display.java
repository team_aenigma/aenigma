package com.aenigma.games.vibrate.model;

import com.badlogic.gdx.math.Vector2;

public class Display
{
    private Vector2 position;
    private float width;
    private float height;

    public Display(float x, float y, float width, float height)
    {
        position = new Vector2(x, y);
        this.width = width;
        this.height = height;
    }

    public Vector2 getPosition()
    {
        return position;
    }

    public Vector2 getSize()
    {
        return new Vector2(width, height);
    }

    public float getWidth()
    {
        return width;
    }

    public float getHeight()
    {
        return height;
    }

}
