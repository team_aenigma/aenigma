package com.aenigma.games.vibrate.model;

import java.util.ArrayList;

public class Digit
{
    private Integer value;
    private ArrayList<Integer> neighbors;

    public Digit(int i)
    {
        value = i;
        setNeighbors();
    }

    private void setNeighbors()
    {
        neighbors = new ArrayList<Integer>();

        switch(value)
        {
            case 1:
                neighbors.add(2);
                neighbors.add(4);
                break;
            case 2:
                neighbors.add(1);
                neighbors.add(3);
                neighbors.add(5);
                break;
            case 3:
                neighbors.add(2);
                neighbors.add(6);
                break;
            case 4:
                neighbors.add(1);
                neighbors.add(7);
                neighbors.add(5);
                break;
            case 5:
                neighbors.add(2);
                neighbors.add(4);
                neighbors.add(6);
                neighbors.add(8);
                break;
            case 6:
                neighbors.add(3);
                neighbors.add(5);
                neighbors.add(9);
                break;
            case 7:
                neighbors.add(4);
                neighbors.add(8);
                break;
            case 8:
                neighbors.add(5);
                neighbors.add(7);
                neighbors.add(9);
                neighbors.add(0);
                break;
            case 9:
                neighbors.add(6);
                neighbors.add(8);
                break;
            case 0:
                neighbors.add(8);
                break;
        }
    }

    public int getValue()
    {
        return value;
    }

    public boolean isNeighbor(int i)
    {
        return neighbors.contains(i);
    }
}
