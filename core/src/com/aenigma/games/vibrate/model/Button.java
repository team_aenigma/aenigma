package com.aenigma.games.vibrate.model;

import com.badlogic.gdx.math.Vector2;

import java.util.ArrayList;

/**
 * Created by Seika on 05/04/2015.
 */
public class Button
{
    private String text;
    private Vector2 textPosition;
    private Vector2 position;
    private float size;

    public Button(String value, Vector2 position, float size)
    {
        this.text = value;
        this.position = position;
        this.size = size;
    }

    public String getText()
    {
        return text;
    }

    public void setText(String text)
    {
        this.text = text;
    }

    public Vector2 getTextPosition()
    {
        return textPosition;
    }

    public void setTextPosition(Vector2 textPosition)
    {
        this.textPosition = textPosition;
    }

    public Vector2 getPosition()
    {
        return position;
    }

    public void setPosition(Vector2 position)
    {
        this.position = position;
    }

    public float getSize()
    {
        return size;
    }

    public void setSize(float size)
    {
        this.size = size;
    }
}
