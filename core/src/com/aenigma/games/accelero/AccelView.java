package com.aenigma.games.accelero;

import com.aenigma.games.GameScene;
import com.aenigma.games.Utils;
import com.aenigma.games.accelero.model.Element;
import com.aenigma.games.accelero.model.ElementType;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;

import java.util.ArrayList;
import java.util.Random;

public class AccelView extends GameScene
{
    BitmapFont font;

    final Color BACKGROUND = Color.valueOf("292929");
    final Color END_ZONE = Color.valueOf("111111");
    final Color TARGET_OK = Color.valueOf("458C02");
    final Color TARGET_OK_END = Color.valueOf("184002");
    final Color TARGET_KO = Color.valueOf("D4280E");
    final Color TARGET_KO_END = Color.valueOf("A51D0C");
    final Color PLAYER = Color.valueOf("F4F4F2");

    public AccelView()
    {
        font = Utils.generateFont("fonts/Square.ttf", 100, Color.WHITE);
    }

    public void render(AccelLogic logic)
    {
        // Set background
        Gdx.gl.glClearColor(BACKGROUND.r, BACKGROUND.g, BACKGROUND.b, BACKGROUND.a);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        // Update scene components such as camera, spriteBatch and shapeRenderer
        updateComponents();

        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);

        // Render end zone
        shapeRenderer.setColor(END_ZONE);
        float playerY = logic.getPlayer().getPosition().y;
        float endHeight = viewport.getWorldHeight() / 2 + playerY;
        shapeRenderer.rect(-viewport.getWorldWidth() / 2, -viewport.getWorldHeight() / 2,
                           viewport.getWorldWidth(), endHeight);

        // Render targets
        for (Element e : logic.getTargets())
        {
            shapeRenderer.setColor(getColor(e, playerY));
            shapeRenderer.rect(e.getPosition().x, e.getPosition().y, e.getSize(), e.getSize());
        }

        // Render player
        shapeRenderer.setColor(PLAYER);
        Element player = logic.getPlayer();
        shapeRenderer.rect(player.getPosition().x, player.getPosition().y, player.getSize(), player.getSize());

        shapeRenderer.end();

        spriteBatch.begin();

        String score = String.valueOf((int) logic.getScore());
        float scoreX = -viewport.getWorldWidth() / 2 + (viewport.getWorldWidth() - font.getBounds(score).width) / 2;
        float scoreY = -viewport.getWorldHeight() / 2 + (endHeight - font.getBounds(score).height) / 2 + font.getBounds(score).height;

        font.draw(spriteBatch, score, scoreX, scoreY);

        spriteBatch.end();
    }

    private Color getColor(Element e, float playerY)
    {
        if (e.getType() == ElementType.TARGET_OK)
        {
            if (e.getPosition().y + e.getSize() < playerY)
                return TARGET_OK_END;
            else
                return TARGET_OK;
        }
        else
        {
            if (e.getPosition().y + e.getSize() < playerY)
                return TARGET_KO_END;
            else
                return TARGET_KO;
        }
    }
}
