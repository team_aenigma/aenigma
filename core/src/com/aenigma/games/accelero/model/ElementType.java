package com.aenigma.games.accelero.model;

public enum ElementType
{
    TARGET_OK,
    TARGET_KO,
    PLAYER
}
