package com.aenigma.games.accelero.model;

import com.badlogic.gdx.math.Vector2;

public class Element
{
    protected Vector2 position;
    protected int size;
    protected float velocity;
    private final float MAX_VELOCITY = 10f;
    private ElementType type;

    public Element(Vector2 position, int size, ElementType type)
    {
        this.position = position;
        this.size = size;
        velocity = 3f;
        this.type = type;
    }

    public Vector2 getPosition()
    {
        return position;
    }

    public int getSize()
    {
        return size;
    }

    public ElementType getType()
    {
        return type;
    }

    public void update()
    {
       velocity = (velocity + 0.05f >= MAX_VELOCITY) ? MAX_VELOCITY : velocity + 0.05f;
       position.y -= velocity;
    }
}
