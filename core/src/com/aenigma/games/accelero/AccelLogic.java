package com.aenigma.games.accelero;

import com.aenigma.games.GameState;
import com.aenigma.games.accelero.model.Element;
import com.aenigma.games.accelero.model.ElementType;
import com.aenigma.games.accelero.model.Player;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;

import java.util.ArrayList;
import java.util.Random;

public class AccelLogic
{
    GameState state;

    private float score;
    private float scoreOutOf;

    private float accelX;
    private float oldAccelX;

    private ArrayList<Element> targets;
    private Player player;
    private final int SIZE = 100;
    private int worldWidth = 0;
    private int worldHeight = 0;

    public AccelLogic()
    {
        state = GameState.INITIALIZING;
    }

    public void setGame(int width, int height)
    {
        accelX = 0;
        oldAccelX = 0;

        score = 0;

        worldWidth = width;
        worldHeight = height;

        targets = generateTargets();
        scoreOutOf = targets.size();

        player = new Player(new Vector2(-SIZE/2, -height/2 + 300), SIZE);
    }

    private ArrayList<Element> generateTargets()
    {
        ArrayList<Element> out = new ArrayList<Element>();

        for (int i = 0; i < 100; i++)
        {
            float minBound = -worldWidth / 2 + SIZE;
            float maxBound = worldHeight / 2 - SIZE;
            float x = (float) (Math.random() * (maxBound - minBound));

            Vector2 v;
            if (new Random().nextInt() % 2 == 0)
                v = new Vector2(-x, i * 200);
            else
                v = new Vector2(x, i * 200);

            ElementType type;

            if (new Random().nextInt() % 4 == 0)
                type = ElementType.TARGET_KO;
            else
                type = ElementType.TARGET_OK;

            int size = (int) (SIZE / 2 + Math.random() * SIZE / 3);
            out.add(new Element(v, size, type));
        }

        return out;
    }

    public void update()
    {
        oldAccelX = accelX;
        accelX = Gdx.input.getAccelerometerX();

        checkCollision();

        if (targets.isEmpty())
        {
            state = GameState.ENDED;
            return;
        }

        updateTargets();
        player.update(oldAccelX, accelX, worldWidth);
    }

    private void checkCollision()
    {
        if (targets.isEmpty())
            return;

        for (int i = 0; i < targets.size(); i++)
        {
            Vector2 posTarget = targets.get(i).getPosition();
            Vector2 posPlayer = player.getPosition();
            int sizeTarget = targets.get(i).getSize();
            int sizePlayer = player.getSize();
            ElementType type = targets.get(i).getType();

            if (posTarget.x < posPlayer.x + sizePlayer
                && posTarget.x + sizeTarget > posPlayer.x
                && posTarget.y < posPlayer.y + sizePlayer
                && posTarget.y + sizeTarget > posPlayer.y)
            {
                if (type == ElementType.TARGET_KO)
                    score = (score - 1 < 0) ? 0 : score - 1;
                else
                    score++;

                targets.remove(i);
            }
        }
    }

    private void updateTargets()
    {
        if (targets.isEmpty())
            return;

        for (int i = 0; i < targets.size(); i++)
        {
            Element e = targets.get(i);
            e.update();
            if (e.getPosition().y < -worldHeight / 2)
            {
                targets.remove(i);
                i--;
            }
        }
    }

    public float getX()
    {
        return accelX;
    }

    public GameState getState()
    {
        return state;
    }

    public ArrayList<Element> getTargets()
    {
        return targets;
    }

    public Element getPlayer()
    {
        return player;
    }

    public float getScore()
    {
        return score;
    }
}
