package com.aenigma.game.android;

import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aenigma.games.android.GameTools;
import com.aenigma.games.android.GameType;
import com.aenigma.navigation.ListMiniGame.MiniGameAdapter;
import com.aenigma.navigation.ListMiniGame.MiniGameListContent;
import com.aenigma.navigation.R;
import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;

import java.util.HashMap;
import java.util.Random;

public class Arcade extends AndroidApplication {

    /**
     * The mini-games list
     */
    ListAdapter mAdapter;

    SessionManager session;
    ArcadeAnim anim;
    Intent game;

    int level = 0;
    int pieces = 0;
    int lives = 4;

    public static HashMap<Integer, Integer> rewards;

    static {
        rewards = new HashMap<>();
        rewards.put(5, 1);
        rewards.put(10, 1);
        rewards.put(14, 1);
        rewards.put(17, 1);
        rewards.put(19, 1);
        for (int i = 20; i < 200; ++i)
            rewards.put(i, 1);
    }

    public Arcade() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.fragment_arcade);

        // have some music
        session = new SessionManager(this);
        // TODO: Change Adapter to display your content
        mAdapter = new MiniGameAdapter(this, MiniGameListContent.ITEMS);

        RelativeLayout rl = (RelativeLayout) findViewById(R.id.arcade_layout);
        AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);

        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.addRule(RelativeLayout.BELOW, R.id.rewardinfo);
        params.topMargin = 50;
        params.width = (int) (size.x * 0.8);
        params.height = size.y / 4;
        params.leftMargin = (size.x - params.width) / 2;

        anim = new ArcadeAnim(params.width, params.height);
        View gameView = initializeForView(anim, config);
        rl.addView(gameView, params);
        anim.init(this, rl.getBackground());
    }

    @Override
    protected void onResume() {
        super.onResume();

        // First game
        if (level == 0) {
            level++;
            TextView levelView = (TextView) findViewById(R.id.level);
            TextView continueBtn = (TextView) findViewById(R.id.arcade_continue);

            levelView.setText(level + "");
            continueBtn.setText(R.string.launch);
        } else {
            // End of a mini-game
            if (rewards.containsKey(level))
                pieces++;
            if (rewards.containsKey(level + 1)) {
                findViewById(R.id.rewardinfo).setVisibility(View.VISIBLE);
                ((TextView) findViewById(R.id.rewardinfo)).setText(R.string.arcade_nextreward);
            }
            else
                findViewById(R.id.rewardinfo).setVisibility(View.INVISIBLE);

            // Set animation depending on result //TODO: not random
            Random rn = new Random();
            int result = rn.nextInt(100);

            if (result >= 50) {
                anim.state = TrainStates.BRAKE;
                level++;
            }
            else {
                anim.state = TrainStates.ACCIDENT;
                lives--;
            }

            // Update textviews
            TextView piecesView = (TextView) findViewById(R.id.pieces);
            TextView levelView = (TextView) findViewById(R.id.level);
            TextView continueBtn = (TextView) findViewById(R.id.arcade_continue);

            piecesView.setText("x " + pieces);
            levelView.setText(level + "");
            continueBtn.setText(R.string.arcade_continue);
        }

        // Update lives
        if (lives < 4)
            findViewById(R.id.life4).setVisibility(View.INVISIBLE);
        if (lives < 3)
            findViewById(R.id.life3).setVisibility(View.INVISIBLE);
        if (lives < 2)
            findViewById(R.id.life2).setVisibility(View.INVISIBLE);
        if (lives < 1) {
            findViewById(R.id.life1).setVisibility(View.INVISIBLE);
            findViewById(R.id.rewardinfo).setVisibility(View.VISIBLE);
            ((TextView) findViewById(R.id.rewardinfo)).setText("GAME OVER");
            findViewById(R.id.arcade_continue).setVisibility(View.INVISIBLE);
            findViewById(R.id.arcade_end).setVisibility(View.INVISIBLE);
            anim.state = TrainStates.END;
        }
    }

    @Override
    public void onBackPressed() {
    }


    public void launchRandomGame(View view) {
        if (lives >= 1) {
            anim.state = TrainStates.GO;
            anim.scaleY = anim.scale;

            Random rn = new Random();
            int id = rn.nextInt(mAdapter.getCount());

            GameType gameType = GameTools.getGameType(MiniGameListContent.ITEMS.get(id).id);
            game = GameTools.getGameIntent(gameType, this);
        }
    }

    public void startGame() {
        startActivity(game);
    }

    public void endArcade(View view) {
        if (lives >= 1)
            session.userLevelUp(pieces);
        this.finish();
    }
}
