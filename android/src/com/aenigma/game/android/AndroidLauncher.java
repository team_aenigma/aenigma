package com.aenigma.game.android;

import android.os.Bundle;
import android.view.View;
import android.widget.TableLayout;
import android.widget.TextView;

import com.aenigma.navigation.R;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;

public class AndroidLauncher extends AndroidApplication {

    boolean isVibrating = false;
    boolean killed = false;

    Controller game;

    TextView accelStats;
    TextView touchStats;
    TextView gyroStats;
    TextView coordStats;
    TextView microStats;
    String accelString;
    String touchString;
    String gyroString;
    String coordString;

    Runnable r;

    @Override protected void onCreate (Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout);

        TableLayout tl = (TableLayout) findViewById(R.id.minigame);
        AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
        game = new Controller();
        View gameView = initializeForView(game, config);
        tl.addView(gameView);
        game.init(this);

        touchStats  = (TextView) findViewById(R.id.touched);
        gyroStats   = (TextView) findViewById(R.id.gyro);
        accelStats  = (TextView) findViewById(R.id.accel);
        coordStats  = (TextView) findViewById(R.id.coord);
        microStats  = (TextView) findViewById(R.id.micro);

        r = new Runnable() {
            public void run() {
                gyroStats.setText(gyroString);
                accelStats.setText(accelString);
                touchStats.setText(touchString);
                coordStats.setText(coordString);

               handler.postDelayed(this, 100);
            }
        };
        handler.postDelayed(r, 100);
    }

    @Override
    protected void onStop() {
        super.onStop();
        //killed = true;
    }

    public void updateAccel(String val) {
        accelString = val;
    }

    public void updateGyro(String val) {
        gyroString = val;
        //System.out.println("updated with " + gyroString);
    }
    public void updateTouch(String val, String coord) {
        touchString = val;
        coordString = coord;
    }

    public void toggleVibrate(View view) {
        if (!isVibrating)
            Gdx.input.vibrate(new long[] { 50, 50, 50, 50}, 0);
        else
            Gdx.input.cancelVibrate();
        isVibrating = !isVibrating;
    }

    public void recordMic(View view) {
        microStats.setText(game.recordMic() + "");
    }
}
