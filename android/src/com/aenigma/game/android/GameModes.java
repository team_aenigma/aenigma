package com.aenigma.game.android;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.aenigma.navigation.R;

public class GameModes extends Activity
{
    public GameModes()
    {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.fragment_gamemodes);
    }

    public void onSelectTraining(View view) {
        Intent i = new Intent(getApplicationContext(), GamesList.class);
        startActivity(i);
    }

    public void onSelectArcade(View view) {
        Intent i = new Intent(getApplicationContext(), Arcade.class);
        startActivity(i);
    }
}
