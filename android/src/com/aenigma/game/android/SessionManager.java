package com.aenigma.game.android;

import java.util.HashMap;
import java.util.Map;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.aenigma.navigation.LoginActivity;
import com.aenigma.navigation.R;
import com.aenigma.tools.save.Save;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.plus.Plus;

import java.util.HashMap;

import static com.google.android.gms.plus.Plus.AccountApi;


public class SessionManager {
    // Core variables
    SharedPreferences pref;
    // Custom editor to handle saves
    SharedPreferences.Editor editor;
    Context context;
    Activity activity;
    public static Resources res;

    // Modes
    int PRIVATE_MODE = 0;
    public static boolean LOCAL_MODE;

    // Sharedpref data
    private static final String PREF_NAME = "AenigmaPref";
    private static final String IS_LOGIN = "IsLoggedIn";
    public static final String KEY_NAME = "name";
    public static final String KEY_EMAIL = "mail";
    public static final String KEY_CONNECTED = "num_connected";
    public static final String KEY_LEVEL = "level";

    // Avatar information
    public static final String KEY_AV_HEAD = "avatar_head";
    public static final String KEY_AV_HAIR = "avatar_hair";
    public static final String KEY_AV_ACC = "avatar_accessory";
    public static Avatar avatar;



    private static GoogleApiClient mGoogleApiClient;

    // Constructor
    public SessionManager(final Activity activity) {
        this.context = activity.getApplicationContext();
        String saved = PREF_NAME;
        if (!LOCAL_MODE)
            saved = LoginActivity.getUserid();
        this.pref = context.getSharedPreferences(saved, PRIVATE_MODE);
        this.editor = pref.edit();
        this.avatar = new Avatar(this);
        this.res = context.getResources();
        this.activity = activity;
    }

    // Getter / Setter
    public static GoogleApiClient getmGoogleApiClient() {
        return mGoogleApiClient;
    }

    public static void setmGoogleApiClient(GoogleApiClient mGoogleApiClient) {
        SessionManager.mGoogleApiClient = mGoogleApiClient;
    }

    /**
     * Create login session
     * */
    public void createLoginSession() {

        System.out.println("LOCAL MODE = " + LOCAL_MODE);

        // Storing login value as TRUE
        editor.putBoolean(IS_LOGIN, true);

        // Storing in prefs
        editor.putString(KEY_NAME, pref.getString(KEY_NAME, ""));
        editor.putString(KEY_CONNECTED, Integer.toString(Integer.parseInt(pref.getString(KEY_CONNECTED, "0")) + 1));
        editor.putString(KEY_LEVEL, pref.getString(KEY_LEVEL, "0"));
        editor.putString(KEY_AV_HEAD, pref.getString(KEY_AV_HEAD, "0"));
        editor.putString(KEY_AV_HAIR, pref.getString(KEY_AV_HAIR, "0"));
        editor.putString(KEY_AV_ACC, pref.getString(KEY_AV_ACC, "0"));

        // Uncomment the following to have the username be the user's real name
        //Person person = Plus.PeopleApi.getCurrentPerson(mGoogleApiClient);
        //String name;
        //if (person != null)
        //   name = person.getDisplayName();
        //else
        //    name = "Anonyme";
        // Storing name in pref
        //editor.putString(KEY_NAME, name);

        avatar.head = Integer.parseInt(pref.getString(KEY_AV_HEAD, "0"));
        avatar.hair = Integer.parseInt(pref.getString(KEY_AV_HAIR, "0"));
        avatar.accessory = Integer.parseInt(pref.getString(KEY_AV_ACC, "0"));

        // commit changes
        saveSession();
    }

    /**
     * Load the sharedPreferences from the single save into Google' servers
     *
     * @param activity Activity on which you want the waiting screen to display on
     */
    public void loadDataFromSave(final Activity activity) {

        if (!LOCAL_MODE) {
            // Progress dialog to make the user wait till his save is loaded
            ProgressDialog progressDialog = new ProgressDialog(activity);
            progressDialog.setMessage("Loading from previous save");
            progressDialog.setCancelable(false);
            progressDialog.setInverseBackgroundForced(false);
            progressDialog.show();
            // When data is loaded
            progressDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    Map<String, Object> loadedData = Save.getInstance().getmDataLoaded();
                    if (loadedData != null) {
                        for (Map.Entry<String, Object> entry : loadedData.entrySet()) {
                            editor.putString(entry.getKey(), entry.getValue().toString());
                        }

                        editor.commit();
                    }

                    chooseUsername(activity);
                    createLoginSession();
                }
            });
            // Start to load data from his save
            Save.getInstance().loadData(progressDialog);
        }
        else {
            chooseUsername(activity);
            createLoginSession();
        }
    }

    public void chooseUsername(Activity activity)
    {
        // On first connection, let the user select his/her username
        if (pref.getString(KEY_CONNECTED, "0").equals("0")) {
            LayoutInflater li = LayoutInflater.from(activity);
            View promptView = li.inflate(R.layout.prompt, null);
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(activity);

            final EditText userInput = (EditText) promptView.findViewById(R.id.textDialogInput);

            alertDialogBuilder.setView(promptView);
            // Set the dialog which allow the user to enter the nickname
            alertDialogBuilder
                    .setCancelable(false)
                    .setPositiveButton(res.getText(R.string.ok),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    editor.putString(KEY_NAME, String.valueOf(userInput.getText()));
                                    saveSession();
                                }
                            })
                    .setNegativeButton(res.getText(R.string.cancel),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });

            // create alert dialog
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        } else {
            editor.putString(KEY_NAME, pref.getString(KEY_NAME, ""));
        }
    }

    /**
     * Check login method wil check user login status
     * If false it will redirect user to login page
     * Else won't do anything
     * */
    public void checkLogin(){
        if (!this.isLoggedIn()){
            // user is not logged in redirect him to Login Activity
            Intent i = new Intent(context, LoginActivity.class);
            // Closing all the Activities
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            // Add new Flag to start new Activity
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            // Staring Login Activity
            context.startActivity(i);
        }
    }


    /**
     * Get stored session data
     * */
    public HashMap<String, String> getUserDetails(){
        HashMap<String, String> user = new HashMap<>();

        user.put(KEY_NAME, pref.getString(KEY_NAME, null));
        user.put(KEY_CONNECTED, pref.getString(KEY_CONNECTED, "0"));
        user.put(KEY_LEVEL,pref.getString(KEY_LEVEL, "0"));
        if (!LOCAL_MODE)
            user.put(KEY_EMAIL, (pref.getString(KEY_EMAIL, null)));

        avatar.head = Integer.parseInt(pref.getString(KEY_AV_HEAD, "0"));
        avatar.hair = Integer.parseInt(pref.getString(KEY_AV_HAIR, "0"));
        avatar.accessory = Integer.parseInt(pref.getString(KEY_AV_ACC, "0"));

        return user;
    }

    public void userLevelUp(int bonus) {
        editor.putString(KEY_LEVEL, String.valueOf(Integer.valueOf(pref.getString(KEY_LEVEL, "0")) + bonus));
        saveSession();
    }

    /**
     * Clear session details
     * */
    public void logoutUser(){
        // TODO : Clear temporary data (if any, and call saveSession)
        saveSession();

        // After logout redirect user to Login Activity
        Intent i = new Intent(context, LoginActivity.class);
        // Closing all the Activities
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        // Add new Flag to start new Activity
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        // Staring Login Activity
        context.startActivity(i);
    }

    /**
     * Quick check for login
     * **/
    // Get Login State
    public boolean isLoggedIn(){
        // TODO : Get rid of this function
        //if (mGoogleApiClient != null)
        //    return mGoogleApiClient.isConnected();
        return false;
    }

    public void updatedAvatar(Avatar updated) {
        this.avatar.head = updated.head;
        this.avatar.hair = updated.hair;
        this.avatar.accessory = updated.accessory;

        editor.putString(KEY_AV_HEAD, String.valueOf(updated.head));
        editor.putString(KEY_AV_HAIR, String.valueOf(updated.hair));
        editor.putString(KEY_AV_ACC, String.valueOf(updated.accessory));

        saveSession();
    }

    public void updateName(String name) {
        editor.putString(KEY_NAME, name);
        saveSession();
    }

    public void saveSession() {
        editor.commit();
        if (!LOCAL_MODE)
            Save.getInstance().saveSnapshot(pref.getAll());
    }

    public void clearAllData() {
        editor.clear();
        saveSession();
    }

    public void logout() {
        clearAllData();
        logoutUser();
    }
}