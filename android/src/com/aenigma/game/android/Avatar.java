package com.aenigma.game.android;

import com.aenigma.navigation.R;

public class Avatar {

    public boolean male = true;

    public int head;
    public int hair;
    public int accessory;

    public final int maxhead = 2;
    public final int maxhair = 2;
    public final int maxacc = 4;

    SessionManager session;

    public Avatar(SessionManager session){
        this.session = session;
    }

    public Avatar(SessionManager session, Avatar clone){
        this.session = session;
        this.head = clone.head;
        this.hair = clone.hair;
        this.accessory = clone.accessory;
        this.male = clone.male;
    }

    public void setHead(int head){
        this.head = head;
    }
    public void setHair(int hair){
        this.head = hair;
    }
    public void setAccessory(int accessory){
        this.head = accessory;
    }

    public int getAvatarView() {
        switch (this.head){
            case 1:
                this.male = false;
                return R.mipmap.female1;
            default:
                this.male = true;
                return R.mipmap.male1;
        }
    }

    public int getHairView() {
        switch (this.hair){
            case 1:
                if (male)
                    return R.mipmap.mhair1;
                else
                    return R.mipmap.fhair1;
            default:
                return R.mipmap.noface;
        }
    }

    public int getAccessoryView() {
        switch (this.accessory){
            case 0:
                return R.mipmap.noface;
            case 1:
                return R.mipmap.accessory1;
            case 2:
                return R.mipmap.accessory2;
            default:
                return R.mipmap.accessory3;
        }
    }

    public void changeHead(int i) {
        this.head += i;
        if (this.head > maxhead)
            this.head = 0;
        if (this.head < 0)
            this.head = maxhead;
    }

    public void changeHair(int i) {
        this.hair += i;
        if (this.hair > maxhair)
            this.hair = 0;
        if (this.hair < 0)
            this.hair = maxhair;
    }

    public void changeAccessory(int i) {
        this.accessory += i;
        if (this.accessory > maxacc)
            this.accessory = 0;
        if (this.accessory < 0)
            this.accessory = maxacc;
    }
}
