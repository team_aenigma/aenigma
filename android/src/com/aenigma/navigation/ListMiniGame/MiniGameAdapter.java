package com.aenigma.navigation.ListMiniGame;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.aenigma.navigation.ListMiniGame.MiniGameListContent.MiniGameItem;
import com.aenigma.navigation.R;

import java.util.List;

/**
 * Created by tis on 28/02/2015.
 */
public class MiniGameAdapter extends ArrayAdapter<MiniGameItem>
{
    public MiniGameAdapter(Context context, List<MiniGameItem> games)
    {
        //android.R.id.text1 was call in third on super
        super(context, android.R.layout.simple_list_item_1, games);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        // Get the data item for this position
        MiniGameItem game = getItem(position);

        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null)
        {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.minigame_item, parent, false);
        }

        // Lookup view for data population
        TextView viewTitle = (TextView) convertView.findViewById(R.id.miniGameTitle);
        TextView viewId = (TextView) convertView.findViewById(R.id.miniGameId);
        // Populate the data into the template view using the data object
        viewTitle.setText(game.title);
        viewId.setText(game.id);
        // Return the completed view to render on screen
        return convertView;
    }
}
