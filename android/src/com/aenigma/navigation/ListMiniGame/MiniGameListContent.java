package com.aenigma.navigation.ListMiniGame;

import android.content.res.Resources;

import com.aenigma.navigation.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Helper class for providing sample content for user interfaces created by
 * Android template wizards.
 * <p/>
 * TODO: Replace all uses of this class before publishing your app.
 */
public class MiniGameListContent {

    /**
     * An array of sample (dummy) items.
     */
    public static List<MiniGameItem> ITEMS = new ArrayList<MiniGameItem>();

    /**
     * A map of sample (dummy) items, by ID.
     */
    public static Map<String, MiniGameItem> ITEM_MAP = new HashMap<String, MiniGameItem>();


    static {
        addItem(new MiniGameItem("Touch", "Game 1", "Description of game 1"));
        addItem(new MiniGameItem("Compass", "Game 2", "Description of game 2"));
        addItem(new MiniGameItem("Vibrate", "Game 3", "Description of game 3"));
        addItem(new MiniGameItem("Accelerometer", "Game 4", "Description of game 4"));
        addItem(new MiniGameItem("Audio", "Game 5", "Description of game 5"));
    }

    private static void addItem(MiniGameItem item) {
        ITEMS.add(item);
        ITEM_MAP.put(item.id, item);
    }

    /**
     * A dummy item representing a piece of content.
     */
    public static class MiniGameItem {
        public String id;
        public String title;
        public String description;
        public Integer nbPiecePuzzle;

        public MiniGameItem(String id, String content, String desc) {
            this.id = id;
            this.title = content;
            this.description = desc;
            this.nbPiecePuzzle = 4;
        }

        @Override
        public String toString() {
            return title;
        }
    }
}
