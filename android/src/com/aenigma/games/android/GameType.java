package com.aenigma.games.android;

public enum GameType
{
    TOUCH,
    COMPASS,
    VIBRATE,
    ACCELEROMETER,
    AUDIO,
}
